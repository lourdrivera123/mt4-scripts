//+------------------------------------------------------------------+
//|                                                 CancelCollar.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   int total = OrdersTotal();
     for(int i=total-1;i>=0;i--)
     {
       if(OrderSelect(i, SELECT_BY_POS)) {
          int type   = OrderType();
      
          bool result = false;
   
          if(OrderSymbol() == Symbol() && type >= 2) {
            result = OrderDelete( OrderTicket() );
          }
       }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                           NormalTradeUSDOnly.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#property script_show_inputs

enum orderType {
   orderType_2=OP_BUYLIMIT,  // Buy Limit
   orderType_3=OP_SELLLIMIT, // Sell Limit
   orderType_4=OP_BUYSTOP,   // Buy Stop
   orderType_5=OP_SELLSTOP,  // Sell Stop
};

input double price;
input orderType OrderType=orderType_2;
input double pips=8;

double volume;
double marketPrice;
double risk=0.03;
double amountRisk;
extern double MaxRiskPerTrade=3;

double points;
double sellstopSLprice;
double buystopSLprice;

double tpPoints;
double sellTPPrice;
double buyTPPrice;


double CalculateLotSize(double SL){          //Calculate the size of the position size 
   double LotSize=0;
   //We get the value of a tick
   double nTickValue=MarketInfo(Symbol(),MODE_TICKVALUE);
   //If the digits are 3 or 5 we normalize multiplying by 10
   if(Digits==3 || Digits==5){
      nTickValue=nTickValue*10;
   }
   //We apply the formula to calculate the position size and assign the value to the variable
   amountRisk = AccountBalance()*MaxRiskPerTrade/100;
   LotSize=(amountRisk)/(SL*nTickValue);
   return LotSize;
}

void PlaceOrder() {
    if(OrderType == orderType_2 || OrderType == orderType_4) {      
      // Place the buy stop order
      int buystop=OrderSend(
         Symbol(),
         OrderType,
         volume,
         marketPrice,
         0,
         buystopSLprice,
         buyTPPrice,
         "BS-"+(string)Symbol()+"-NT",
         1,
         0,
         clrGreen
      );
   } else if (OrderType == orderType_3 || OrderType == orderType_5) {
      // Place the sell stop order
      int sellstop=OrderSend(
         Symbol(),
         OrderType,
         volume,
         marketPrice,
         0,
         sellstopSLprice,
         sellTPPrice,
         "SS-"+(string)Symbol()+"-NT",
         1,
         0,
         clrRed
      );
   }
}

//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   marketPrice = price;

   volume = CalculateLotSize(pips);
   
   // Multiply the pips by 10 and multiply by the Point on current pair (JPY)
   points = (pips*10)*Point;
   sellstopSLprice=NormalizeDouble(marketPrice+points,Digits);
   buystopSLprice=NormalizeDouble(marketPrice-points,Digits);
   
   tpPoints = ((pips*1.5)*10)*Point;
   sellTPPrice = NormalizeDouble(marketPrice-tpPoints,Digits);
   buyTPPrice=NormalizeDouble(marketPrice+tpPoints,Digits);
   
   PlaceOrder();
  }  
//+------------------------------------------------------------------+

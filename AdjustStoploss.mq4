//+------------------------------------------------------------------+
//|                                               AdjustStoploss.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

int points=1;

//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   double stoploss = 0.0;
   
   if(OrderType() == 0) {
      stoploss = NormalizeDouble(OrderOpenPrice()+((points*10)*Point),Digits);
   } else if(OrderType() == 1) {
      stoploss = NormalizeDouble(OrderOpenPrice()-((points*10)*Point),Digits);
   }
   
   for( int i = 0 ; i < OrdersTotal() ; i++ ) {
            if(OrderSelect( i, SELECT_BY_POS, MODE_TRADES )) {
               if(OrderModify(OrderTicket(),OrderOpenPrice(),stoploss,OrderTakeProfit(),0,Blue)) {
                  //Alert("Order modified");
               }
            }
         }
  }
//+------------------------------------------------------------------+

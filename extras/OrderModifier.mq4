//+------------------------------------------------------------------+
//|                                                OrderModifier.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
if(isProtectionActivated == false) {
   for( int i = 0 ; i < OrdersTotal() ; i++ ) {
      if(OrderSelect( i, SELECT_BY_POS, MODE_TRADES )) {
         if(OrderModify(OrderTicket(),OrderOpenPrice(),NormalizeDouble(OrderOpenPrice()-((1*10)*Point),Digits),OrderTakeProfit(),0,Blue)) {
            isProtectionActivated = true
         }
      }
   }
 }
  }
//+------------------------------------------------------------------+

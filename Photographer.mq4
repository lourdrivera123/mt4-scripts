//+------------------------------------------------------------------+
//|                                                 Photographer.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+

extern string FileName = "mydata.csv";

string s[10][1000];

void readCSV() {
   int row=0,col=0;
   int rowCnt,colCnt;
   int handle=FileOpen(FileName,FILE_CSV|FILE_READ,",");
   if(handle>0)
   {
     while(True)
     {
       string temp = FileReadString(handle);
       if(FileIsEnding(handle)) break;
       s[col][row]=temp;       
       if(FileIsLineEnding(handle))
       {
         colCnt = col;
         col = 0;
         row++;
       }
       else
       {
         col++;
       }
       rowCnt = row-1;
     }
     FileClose(handle);
     string lines = "  ";
     for(row=0; row<=rowCnt; row++)
     {
       for(col=0; col<=colCnt; col++)  
       {
         lines = lines + " " + s[col][row];
       }
       lines = lines + "\n";
     }
     Comment(lines);
   }
   else
   {
     Comment("File "+FileName+" not found, the last error is ", GetLastError());
   }
}

void OnStart()
  {
//---
readCSV();
/**
   string filename = "chartscreenshot"+IntegerToString(candleTime)+".png";
      if(ChartScreenShot(0,filename,800,600,ALIGN_LEFT)) {
            int fh = FileOpen("file.csv", FILE_CSV|FILE_READ|FILE_WRITE,',');
            Print(fh);
            FileWrite(fh, pair,filename,"LONG",false);
            FileFlush(fh);
            FileClose(fh);
   
            ShellExecuteW(0,"open","c:\\Users\\Administrator\\AppData\\Roaming\\MetaQuotes\\Terminal\\3294B546D50FEEDA6BF3CFC7CF858DB7\\MQL4\\Files\\jarvisfxmessenger.bat","","",1);
      }
      **/
  }
//+------------------------------------------------------------------+

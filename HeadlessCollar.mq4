//+------------------------------------------------------------------+
//|                                                       Collar.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

input double lots=0;
input double pips=6;
input double price;

double marketPrice;
double volume;
double risk=0.03;
extern double MaxRiskPerTrade=3;
double amountRisk;

double CalculateLotSize(double SL){          //Calculate the size of the position size 
   double LotSize=0;
   //We get the value of a tick
   double nTickValue=MarketInfo(Symbol(),MODE_TICKVALUE);
   //If the digits are 3 or 5 we normalize multiplying by 10
   if(Digits==3 || Digits==5){
      nTickValue=nTickValue*10;
   }
   //We apply the formula to calculate the position size and assign the value to the variable
   amountRisk = AccountBalance()*MaxRiskPerTrade/100;
   LotSize=(amountRisk)/(SL*nTickValue);
   return LotSize;
}

bool CheckOpenOrders(){
   //We need to scan all the open and pending orders to see if there is there is any
   //OrdersTotal return the total number of market and pending orders
   //What we do is scan all orders and check if they are of the same symbol of the one where the EA is running
   for( int i = 0 ; i < OrdersTotal() ; i++ ) {

      //We select the order of index i selecting by position and from the pool of market/pending trades
      if(OrderSelect( i, SELECT_BY_POS, MODE_TRADES )) {
         int type   = OrderType();
         //If the pair of the order (OrderSymbol() is equal to the pair where the EA is running (Symbol()) then return true
         if( OrderSymbol() == Symbol() && type <= 1 ) return(true);
      }
   }
   //If the loop finishes it mean there were no open orders for that pair
   return(false);
}

void DeletePendingOrders() {
 int total = OrdersTotal();
  for(int i=total-1;i>=0;i--)
  {
    if(OrderSelect(i, SELECT_BY_POS)) {
       int type   = OrderType();
   
       bool result = false;

       if(OrderSymbol() == Symbol() && type >= 2) {
         result = OrderDelete( OrderTicket() );
       }
    }
  }
}

//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
  if(CheckOpenOrders() == true) {
   DeletePendingOrders();
   MessageBox("Ongoing trade. Pending orders not allowed.", "Denied");
   return;
  }

//--
   // If volume is 0, calculate the volume by 3% risk.
   // (Net liquidation * Risk percentage)/(Stop loss pips * Value per pip)
   // Value per pip for USD = $10
   string quoteCurrency = StringSubstr((string)Symbol(), 3, 3);
   string baseCurrency = StringSubstr((string)Symbol(), 0, 3);

   if(lots == 0) {
      volume = CalculateLotSize(pips);      
   } else {
      volume = lots;
   }

   /**
   if(lots == 0 && quoteCurrency == "USD") {
      double noOfLots = (AccountBalance()*risk)/(pips*10);
      volume = NormalizeDouble(noOfLots, 2);
   } else if(lots == 0 && baseCurrency == "USD") {
      // Value per pip is the $10/Ask
      double noOfLots = (AccountBalance()*risk)/(pips*(10/Ask));
      volume = NormalizeDouble(noOfLots, 2);
   } else if(lots == 0 && quoteCurrency != "USD") {
      MessageBox("Lot Size Required", "Invalid Order");
      return;
   } else {
      volume = lots;
   }
   **/


   // If price is left empty, calculate the market price
   if(price==0) {
    marketPrice = NormalizeDouble(((Ask+Bid)/2),(Digits-1));
   } else {
    marketPrice = price;
   }
   
   // Multiply the pips by 10 and multiply by the Point on current pair (JPY)
   double points = (pips*10)*Point;
   double sellstopprice=NormalizeDouble(Bid-points,Digits);
   double buystopprice=NormalizeDouble(Ask+points,Digits);
   
   // Place the sell stop order
   int sellstop=OrderSend(
      Symbol(),
      OP_SELLSTOP,
      volume,
      sellstopprice,
      0,
      Bid,
      0,
      "SS-"+(string)Symbol()+"-NS",
      1,
      0,
      clrRed
   );
   
   //if(sellstop<0) { Print("OrderSend failed with error #",GetLastError()); }else{ Print("OrderSend placed successfully"); };
      
   // Place the buy stop order
   int buystop=OrderSend(
      Symbol(),
      OP_BUYSTOP,
      volume,
      buystopprice,
      0,
      Ask,
      0,
      "BS-"+(string)Symbol()+"-NS",
      1,
      0,
      clrGreen
   );
 
   //if(buystop<0) { Print("OrderSend failed with error #",GetLastError()); } else { Print("OrderSend placed successfully"); }
  }
//+------------------------------------------------------------------+
